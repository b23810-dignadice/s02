-- List down the databases in the DBMS
SHOW DATABASES;

-- Create a database
CREATE DATABASE music_db;

-- Drop (delete) a databasse
DROP DATABASE music_db;

-- Select a database
USE music_db;

 -- Create tables 
 CREATE TABLE users(
    id INT NOT NULL AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    full_name VARCHAR(50) NOT NULL,
    contact_number INT NOT NULL,
    email VARCHAR(50),
    address VARCHAR(50),
    PRIMARY KEY (id)
 );


 CREATE TABLE artists(
   id INT NOT NULL AUTO_INCREMENT,
   name VARCHAR(50) NOT NULL,
   PRIMARY KEY (id)
 );

 CREATE TABLE albums(
   id INT NOT NULL AUTO_INCREMENT, 
   album_title VARCHAR NOT NULL,
   date_released DATE NOT NULL,
   artist_id INT NOT NULL,
   PRIMARY KEY(id),
   CONSTRAINT fk_albums_artist_id -- SQL constraints are used to specify rules for the data in a table.
      FOREIGN KEY(artist_id) REFERENCES artists(id)
      ON UPDATE CASCADE -- automatically updates the linked ID 
      ON DELETE RESTRICT
 );


 artists: 
 id artist 
 1 linkin park

 albums
 id album_title artist_id
 1   meteora     1
 1   meteora     1

 CREATE TABLE songs(
   id INT NOT NULL AUTO_INCREMENT,
   song_name VARCHAR(50) NOT NULL,
   length TIME NOT NULL,
   genre VARCHAR(50) NOT NULL,
   album_id INT NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT fk_songs_album_id
      FOREIGN KEY (album_id) REFERENCES albums(id)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
 )

 id  album_title 
 1    meteora

 song_name album_id
 numb        1


 CREATE TABLE playlists(
   id INT NOT NULL AUTO_INCREMENT,
   user_id int NOT NULL,
   datetime_created DATETIME NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT fk_playlists_user_id
      FOREIGN KEY (user_id) REFERENCES users(id)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
 );

 CREATE TABLE playlist_songs(
   id INT NOT NULL AUTO_INCREMENT,
   playlist_id int NOT NULL,
   song_id int NOT NULL,
   PRIMARY KEY (id),
   CONSTRAINT fk_playlist_songs_playlist_id
      FOREIGN KEY (playlist_id) REFERENCES playlists(id)
      ON UPDATE CASCADE
      ON DELETE RESTRICT,
   CONSTRAINT fk_playlist_songs_songs_id
      FOREIGN KEY (song_id) REFERENCES songs(id)
      ON UPDATE CASCADE
      ON DELETE RESTRICT
 );